//
//  AirportsViewModel.swift
//  AlboTestiOS
//
//  Created by Sergio Rodriguez on 26/04/21.
//

import Foundation
import CoreLocation
class AirportsViewModel: Decodable{
    private var serviceManager : WebServiceManager?
    private let URLRequestAirportsEndpoint = "/airport/nearby?"
    
    public var airportsList : DynamicType<[Airport]> = DynamicType([Airport]())
    public var serviceError     : ((Error) -> ())?
    
    public typealias GPSLocation = (latitude : Float, longitude: Float)
    public func fetchAirports(_ location : GPSLocation?){
        DispatchQueue.global(qos: .default).async {
            let endpoint = self.URLRequestAirportsEndpoint + "longitude=\(location?.longitude ?? 0)" + "&latitude=\(location?.latitude ?? 0)"
            var resource = Resource<[Airport]>(endpoint)
            resource.httpMethod     = .get
            resource.showProgress   = true
            self.serviceManager?.loadService(resource: resource, completion: {[weak self] result in
                guard let self = self else{ return }
                DispatchQueue.main.async {
                    switch result{
                    case .success(let result):
                        self.airportsList.value = result
                    case .failure(let error):
                    if let error = error{
                        print(error.localizedDescription)
                    }
                    }
                }
            })
        }
    }
    required init(from decoder: Decoder) throws {}
    public init(_ serviceManager : WebServiceManager = WebServiceManager()){
        self.serviceManager = serviceManager
    }
    public func shouldShowAirport(_ airport : Airport, _ usrLocation: AirportsViewModel.GPSLocation?, _ selectedDistanceInKilometers: Float) -> Bool{
        let measure = Measurement(value: Double(selectedDistanceInKilometers), unit: UnitLength.kilometers)
        let measureInMeters = measure.converted(to: UnitLength.meters)
        
        let coordinate1 = CLLocation(latitude: CLLocationDegrees(airport.latitude.value ?? 0.0), longitude: airport.longitude.value ?? 0.0)
        let coordinate2 = CLLocation(latitude: CLLocationDegrees(usrLocation?.latitude ?? 0.0), longitude: CLLocationDegrees(usrLocation?.longitude ?? 0.0))
        
        let distanceInMeters = coordinate1.distance(from: coordinate2)
        
        return measureInMeters.value > distanceInMeters
    }
    public func getNumberOfRowsInTableView() -> Int{
        return airportsList.value.count
    }
    public func getAirportAtIndex(_ index: Int) -> Airport{
        return airportsList.value[index]
    }
}
public struct Airport : Codable{
    public var airportName          : DynamicType<String?>
    public var cityCode             : DynamicType<String?>
    public var cityName             : DynamicType<String?>
    public var countryCode          : DynamicType<String?>
    public var country              : DynamicType<String?>
    public var longitude            : DynamicType<Double?>
    public var latitude             : DynamicType<Double?>
    public var distanceInKilometers : DynamicType<Int?>
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        airportName             = DynamicType(try (container.decodeIfPresent(String.self,   forKey: .airportName)   ?? nil))
        cityCode                = DynamicType(try (container.decodeIfPresent(String.self,   forKey: .cityCode)      ?? nil))
        cityName                = DynamicType(try (container.decodeIfPresent(String.self,   forKey: .cityName)      ?? nil))
        countryCode             = DynamicType(try (container.decodeIfPresent(String.self,   forKey: .countryCode)   ?? nil))
        country                 = DynamicType(try (container.decodeIfPresent(String.self,   forKey: .country)       ?? nil))
        longitude               = DynamicType(try (container.decodeIfPresent(Double.self,   forKey: .longitude)     ?? nil))
        latitude                = DynamicType(try (container.decodeIfPresent(Double.self,   forKey: .latitude)      ?? nil))
        distanceInKilometers    = DynamicType(try (container.decodeIfPresent(Int.self,      forKey: .distanceInKilometers) ?? nil))
    }

    private enum CodingKeys: String, CodingKey{
        case airportName            = "name"
        case cityCode               = "code"
        case cityName               = "city"
        case countryCode            = "countryIso"
        case country                = "country"
        case longitude              = "longitude"
        case latitude               = "latitude"
        case distanceInKilometers   = "distance"
    }
}
