//
//  AlboTabBarController.swift
//  AlboTestiOS
//
//  Created by Sergio Rodriguez on 26/04/21.
//

import UIKit

class AlboTabBarController: UITabBarController {
    public var selectedDistanceInKilometers : Float?
    public var usrLocation : AirportsViewModel.GPSLocation?
    lazy var viewModel = AirportsViewModel()
}
