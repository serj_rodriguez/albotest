//
//  ProgressViewGeneric.swift
//  AlboTestiOS
//
//  Created by Sergio Rodriguez on 26/04/21.
//

import Foundation
import UIKit

public class ProgressViewGeneric: UIView{
    
    static let tag = 999999
    
    public static func showProgress(){
        if let window = UIApplication.shared.keyWindow{
            let indicator = UIActivityIndicatorView(style: .large)
            indicator.frame = CGRect(x: 0, y: 0, width: CGFloat(50.0), height: CGFloat(50.0))
            indicator.tag = tag
            window.addSubview(indicator)
            window.bringSubviewToFront(indicator)
        }
    }
    public static func removeProgress(){
        if let window = UIApplication.shared.keyWindow{
            if let viewToRemove = window.viewWithTag(tag){
                DispatchQueue.main.async {
                    viewToRemove.removeFromSuperview()
                }
            }
        }
    }
}
