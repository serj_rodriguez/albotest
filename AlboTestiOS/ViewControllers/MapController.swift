//
//  MapController.swift
//  AlboTestiOS
//
//  Created by Sergio Rodriguez on 26/04/21.
//

import UIKit
import GoogleMaps
class MapController: UIViewController {
    //MARK: IBOutlets & vars
    var mapView : GMSMapView?
    var tabController : AlboTabBarController?
    
    private var locationManager = CLLocationManager()
    //MARK: App Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        if let tabController = self.tabBarController as? AlboTabBarController{
            self.tabController = tabController
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationItem.title = "Airports Map"
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let usrLocation = tabController?.usrLocation{
            tabController?.viewModel.fetchAirports(usrLocation)
        }
        tabController?.viewModel.airportsList.bind {[weak self] airports in
            guard let self = self else{ return }
            for airport in airports{
                if let tabController = self.tabController, (tabController.viewModel.shouldShowAirport(airport, tabController.usrLocation, tabController.selectedDistanceInKilometers ?? 0.0)){
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2D(latitude: airport.latitude.value ?? 0.0, longitude: airport.longitude.value ?? 0.0)
                    marker.title = airport.airportName.value
                    marker.snippet = "\(airport.distanceInKilometers.value ?? 0) kilometers"
                    marker.map = self.mapView
                }
            }
        }
    }
}
//MARK: CLLocationManagerDelegate
extension MapController : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(location.latitude) \(location.longitude)")
        tabController?.usrLocation = (latitude: Float(location.latitude), longitude: Float(location.longitude))
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude, longitude: location.longitude, zoom: 12.0)
        self.mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        self.view.addSubview(mapView ?? UIView())
    }
}
