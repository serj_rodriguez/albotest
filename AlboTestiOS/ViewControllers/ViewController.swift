//
//  ViewController.swift
//  AlboTestiOS
//
//  Created by Sergio Rodriguez on 23/04/21.
//

import UIKit

class ViewController: UIViewController {
    //MARK: IBOutlets & vars
    @IBOutlet weak var btnSearch: UIButton!{
        didSet{
            btnSearch.layer.cornerRadius = CGFloat(15.0)
        }
    }
    @IBOutlet weak var lblKilometers: UILabel!
    @IBOutlet weak var sliderKilometers: UISlider!{
        didSet{
            sliderKilometers.value = 1
            sliderKilometers.minimumValue = 1
            sliderKilometers.maximumValue = 150
        }
    }
    lazy var viewModel = AirportsViewModel()
    var location : AirportsViewModel.GPSLocation?
    //MARK: App Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblKilometers.text = "\(Int(sliderKilometers.minimumValue))"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    //MARK: IBActions
    @IBAction func searchButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: ViewController.self))
        guard let vc = storyboard.instantiateViewController(identifier: "AlboTabBarController") as? AlboTabBarController else { return }
        vc.selectedDistanceInKilometers = sliderKilometers.value
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        lblKilometers.text = "\(currentValue)"
    }
}

