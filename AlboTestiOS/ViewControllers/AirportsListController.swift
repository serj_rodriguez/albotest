//
//  AirportsListController.swift
//  AlboTestiOS
//
//  Created by Sergio Rodriguez on 26/04/21.
//

import UIKit

class AirportsListController: UIViewController {
    //MARK: IBOutlets & vars
    @IBOutlet weak var tblAirports: UITableView!{
        didSet{
            tblAirports.delegate    = self
            tblAirports.dataSource  = self
        }
    }
    
    var tabBar : AlboTabBarController?
    //MARK: App lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationItem.title = "Airports List"
        if let tabBar = self.tabBarController as? AlboTabBarController{
            self.tabBar = tabBar
            self.tblAirports.reloadData()
        }
    }
}
//MARK: UITableViewDelegate & UITableViewDataSource
extension AirportsListController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tabBar?.viewModel.getNumberOfRowsInTableView() ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil{ cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell") }
        guard let airport = tabBar?.viewModel.getAirportAtIndex(indexPath.row),
              let tabController = self.tabBar,
              (tabController.viewModel.shouldShowAirport(airport, tabController.usrLocation, tabController.selectedDistanceInKilometers ?? 0.0)) else { return UITableViewCell() }
        
        cell?.textLabel?.text = airport.airportName.value
        cell?.detailTextLabel?.text = "A \(airport.distanceInKilometers.value ?? 0) kilometros"
        return cell ?? UITableViewCell()
    }
}
