//
//  WebService.swift
//  AlboTestiOS
//
//  Created by Sergio Rodriguez on 26/04/21.
//

import Foundation

public class DynamicType<T> : Codable where T : Codable{
    
    public typealias Listener = (T) -> ()
    public var listener : Listener?
    
    public var value : T{
        didSet{
            listener?(value)
        }
    }
    public init(_ value: T){
        self.value = value
    }
    public func bind(listener: @escaping Listener){
        self.listener = listener
        self.listener?(self.value)
    }
    private enum CodingKeys: CodingKey{
        case value
    }
}

public enum NetworkError: Error{
    case decodingError
    case domainError
    case urlError
}
public enum HTTPMethod : String{
    case get = "GET"
}
public struct Resource<T : Codable>{
    let url             : URL?
    var httpMethod      : HTTPMethod = .get
    var showProgress    : Bool = false
    var body            : Data? = nil
    var parameters      : [String:Any]?
    
    public init(_ endpoint: String?){
        guard let serverStringURL = Bundle(for: WebServiceManager.self).object(forInfoDictionaryKey: "SERVERURL") as? String else{
            self.url = nil
            return
        }
        let finalURLString = "\(serverStringURL)\(endpoint ?? "")"
        self.url = URL(string: finalURLString) ?? nil
    }
}

public enum GenericResult<T,U: Error>{
    case success(T)
    case failure(U?)
}
public final class WebServiceManager{
    public func loadService<T>(resource: Resource<T>, completion: @escaping (GenericResult<T, NetworkError>) -> ()){
        if resource.showProgress{
            DispatchQueue.main.async{
                ProgressViewGeneric.showProgress()
            }
        }
        if let url = resource.url{
            var request = URLRequest(url: url)
            request.httpMethod = resource.httpMethod.rawValue
            if let dicParams = resource.parameters{
                request.httpBody = try? JSONSerialization.data(withJSONObject: dicParams, options: .prettyPrinted)
            }else{
                request.httpBody = resource.body
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("4239f581ebmsh3202752baa0dc05p11393bjsnd1dc36f74729", forHTTPHeaderField: "x-rapidapi-key")
            request.addValue("nearby-airport.p.rapidapi.com", forHTTPHeaderField: "x-rapidapi-host")
            URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data else{
                    DispatchQueue.main.async{
                        ProgressViewGeneric.removeProgress()
                        completion(.failure(.none))
                    }
                    return
                }
                //                print("Response JSON ===> \(String(data: data, encoding: String.Encoding.utf8) ?? "")")
                if let result = try? JSONDecoder().decode(T.self, from: data){
                    DispatchQueue.main.async{
                        ProgressViewGeneric.removeProgress()
                        completion(.success(result))
                    }
                }else{
                    DispatchQueue.main.async {
                        ProgressViewGeneric.removeProgress()
                        completion(.failure(.decodingError))
                    }
                }
            }.resume()
        }
    }
    public init(){}
}
