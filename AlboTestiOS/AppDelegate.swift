//
//  AppDelegate.swift
//  AlboTestiOS
//
//  Created by Sergio Rodriguez on 23/04/21.
//

import UIKit
import GoogleMaps

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyBNllyP0lunoBa46NMa8iZQRftuDAivPM4")
        return true
    }
}

